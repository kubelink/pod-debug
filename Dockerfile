FROM rockylinux:9

# Environment
ENV APP_ROOT /
ENV LOG_DIR $APP_ROOT/log
ENV PS1 "\n\n>> pod-debug \W \$ "
ENV ONDOCKER 1
WORKDIR $APP_ROOT
ENV PATH $APP_ROOT/bin:$PATH
ENV TERM=xterm

# Package Repositories
RUN dnf install -y \
            dnf-utils \
            https://rpms.remirepo.net/enterprise/remi-release-9.rpm \
            epel-release \
    && dnf -y --setopt=tsflags=nodocs update \
    && dnf -y clean all

# Package Repositories


# Packages
ENV PACKAGES \
    python3-pip \
    git \
    unzip \
    duplicity \
    wget \
    net-tools \
    bind-utils \
    telnet \
    xz \
    iputils \
    procps

RUN dnf -y --setopt=tsflags=nodocs install $PACKAGES \
    && dnf clean all

# Install kubectl
RUN cd /usr/local/bin/ && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl

# Command line utilities
RUN pip3 install awscli

## Set permissions for application
RUN chown -R 1001:root /var/log /run
RUN chmod -R g=u /etc/passwd /var/log /run /usr/local/bin/*
RUN chmod +x  /usr/local/bin/*

# Ready
USER 1001
CMD ["/usr/bin/sleep","1h"]
